- Feature Name: Command Protocol
- Start Date: 2018-07-16
- Tracking Issue: 0005

# Summary
[Summary]: #summary

We need a defined protocol for how the client will communicate commands entered by the user to the server.

# Motivation
[motivation]: #motivation

Having a chat based command system for debugging and user interaction will be beneficial in the long run for testing basic features without UI implementations.

We also want commands to be extendable on the server side with mods, therefore command parsing needs to be handled on the server. However some commands may be client specific e.g. `/settings render-distance 10` could be used for quick changes to settings prior to a full UI settings implementation.

# Guide-level explanation
[guide-level-explanation]: #guide-level-explanation

Commands are entered by the user into their client using a specified structure. e.g. `/command param1 param2 param3`

The client will parse this command (could be client specific command structure) and populate a CommandDetail struct with the necessary parts broken up e.g. `{command: "command", "params": ["param1", "param2", "param3"]}`

Commands will then be checked against the list of client commands (A defined list of commands specific to each client interface e.g. Headless, Voxygen) and executed if the client can handle the command. 

If the command is unknown to the client, it will be dispatched to the server over a ClientMessage (`CommandMessage { command, params }`) where the server will check against its own list of server commands and execute the command appropriately. If the command is unknown, the server will respond with an approprate message to alert the user.

The server commands are provided with an API for accessing server objects (entities, players, inventories etc).

# Reference-level explanation
[reference-level-explanation]: #reference-level-explanation

This feature will require access to some of the servers internals. Primarily, the commands will need access to a variety of objects such as entities, players, inventories etc.

[Needs more thought on how this will be implemented]

# Drawbacks
[drawbacks]: #drawbacks

- This will require further extension of the ClientMessage and ServerMessage enums. 
- Commands will need access to a currently undefined API of server internals.

# Rationale and alternatives
[alternatives]: #alternatives

This approach allows for the most flexibility on both the client side for defining command structure to the user and the modding capability on the server side to extend the commands available on a server.

An alternative approach would be to simply forward the chat message onto the server without any special treatment based on special command structure. However this will lead to a more complex chat message handler on the server side and restricts clients to a set command structure for the user.

# Prior art
[prior-art]: #prior-art

Many games provide a chat based command interface which allows for both headless and GUI based clients to send commands to the server.

[Further research into how other games implement this needs to be done]

# Unresolved questions
[unresolved]: #unresolved-questions

- How the interface to the server level variables will work?
- What sort of interface mods will have towards their own command structs?
